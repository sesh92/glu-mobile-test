let express = require('express');
let redis = require('redis');
let bodyParser = require('body-parser');
let app = express();
let system = require('./core/system');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.post('/handler', system.process);

app.modules = system.initModules();

app.start = () => {
  app.redis = redis.createClient({ host : config.redis.host, port : config.redis.port });
	app.listen(config.serverSettings.port, function () {
	  console.log('Example app listening on port', config.serverSettings.port);
	});
}

module.exports = app;
