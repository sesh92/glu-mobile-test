Object.assign(global, {
  app: module,
  config: require('./config.json'),
  server: require('./server'),
});
server.start();
