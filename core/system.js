var requireAll = require('require-all');
var path = require('path');

module.exports.initModules = () => {
  return requireAll(path.resolve(__dirname, './modules'));
}

module.exports.process = (req, res) => {
  let func = req.body.function;
  let type = req.body.type;

  if(!func || !server.modules[type] || !type || !server.modules[type][func]) return res.send('params wrong');

  server.modules[type][func](res, req.body);
}
