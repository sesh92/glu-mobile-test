module.exports.ping = (res, data) => {
  if(!data.userId) return res.send('params wrong');

  server.redis.incr('pong:'+data.userId, (err, value) => {
    if(err) return res.send('redis err');

    res.send('PONG ' + value);
  })
}
