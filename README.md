Install nodejs:

https://nodejs.org/en/download/package-manager/


Install redis:

https://redis.io/topics/quickstart


start server:

npm i
node app.js

check pong n:

curl http://localhost:3000/handler -H "Content-Type: application/json" -X POST -d '{"type":"tools","function":"ping", "userId": "test"}'

JSON package format:
{
	"type": "tools", 	// is required
	"function": "ping", // is required
	"userId": "test" 	// is required
}